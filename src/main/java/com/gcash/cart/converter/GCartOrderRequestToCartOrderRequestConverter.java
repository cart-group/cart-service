package com.gcash.cart.converter;

import java.util.List;

import org.springframework.core.convert.converter.Converter;

import com.gcash.cart.dto.CartOrderRequest;
import com.gcash.cart.dto.OrderRequest;
import com.gcash.cart.grpc.GCartOrderRequest;

public class GCartOrderRequestToCartOrderRequestConverter implements Converter<GCartOrderRequest, CartOrderRequest>{

	@Override
	public CartOrderRequest convert(GCartOrderRequest source) {
		List<OrderRequest> orders = source.getOrdersList().stream().map(order -> {
			OrderRequest orderRequest = new OrderRequest();
			orderRequest.setProductId(order.getProductId());
			orderRequest.setQuantity(order.getQuantity());
			return orderRequest;
		}).toList();
		
		CartOrderRequest cartOrderRequest = new CartOrderRequest();
		cartOrderRequest.setCartId(source.getId());
		cartOrderRequest.setOrders(orders);
		
		return cartOrderRequest;
	}

}
