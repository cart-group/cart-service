package com.gcash.cart.converter;

import org.springframework.core.convert.converter.Converter;

import com.gcash.cart.dto.ResponseOrder;
import com.gcash.cart.grpc.GOrderResponse;

public class ResponseOrderToGOrderResponseConverter implements Converter<ResponseOrder, GOrderResponse>{

	@Override
	public GOrderResponse convert(ResponseOrder source) {
		return GOrderResponse.newBuilder()
				.setId(source.getId())
				.setProductId(source.getProductId())
				.setQuantity(source.getQuantity())
				.build();
	}

}
