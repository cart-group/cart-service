package com.gcash.cart.service;

import com.gcash.cart.grpc.GCartOrderRequest;
import com.gcash.cart.grpc.GCartOrderResponse;
import com.gcash.cart.grpc.GCheckoutCartRequest;
import com.gcash.cart.grpc.GDeleteFromCartRequest;
import com.gcash.cart.grpc.GUpdateCartRequest;

public interface CartService {
	GCartOrderResponse viewCart(String cartId);
	
	GCartOrderResponse addToCart(GCartOrderRequest request);
	
	GCartOrderResponse updateCart(GUpdateCartRequest request);
	
	GCartOrderResponse removeFromCart(GDeleteFromCartRequest request);
	
	GCartOrderResponse checkoutCart(GCheckoutCartRequest request);
}