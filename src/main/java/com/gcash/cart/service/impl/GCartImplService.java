package com.gcash.cart.service.impl;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcash.cart.grpc.GCartOrderRequest;
import com.gcash.cart.grpc.GCartOrderResponse;
import com.gcash.cart.grpc.GCartServiceGrpc.GCartServiceImplBase;
import com.gcash.cart.service.CartService;
import com.gcash.cart.grpc.GCheckoutCartRequest;
import com.gcash.cart.grpc.GDeleteFromCartRequest;
import com.gcash.cart.grpc.GUpdateCartRequest;
import com.gcash.cart.grpc.GViewCartRequest;

import io.grpc.stub.StreamObserver;
import net.devh.boot.grpc.server.service.GrpcService;

@GrpcService
public class GCartImplService extends GCartServiceImplBase{
	
	@Autowired
	private CartService cartService;

	@Override
	public void addToCart(GCartOrderRequest request, StreamObserver<GCartOrderResponse> responseObserver) {
		GCartOrderResponse response = cartService.addToCart(request);
		responseObserver.onNext(response);
		responseObserver.onCompleted();
	}

	@Override
	public void viewCart(GViewCartRequest request, StreamObserver<GCartOrderResponse> responseObserver) {
		GCartOrderResponse response = cartService.viewCart(request.getId());
		responseObserver.onNext(response);
		responseObserver.onCompleted();
	}

	@Override
	public void deleteFromCart(GDeleteFromCartRequest request, StreamObserver<GCartOrderResponse> responseObserver) {
		GCartOrderResponse response = cartService.removeFromCart(request);
		responseObserver.onNext(response);
		responseObserver.onCompleted();
	}

	@Override
	public void updateCart(GUpdateCartRequest request, StreamObserver<GCartOrderResponse> responseObserver) {
		GCartOrderResponse response = cartService.updateCart(request);
		responseObserver.onNext(response);
		responseObserver.onCompleted();
	}

	@Override
	public void checkoutCart(GCheckoutCartRequest request, StreamObserver<GCartOrderResponse> responseObserver) {
		GCartOrderResponse response = cartService.checkoutCart(request);
		responseObserver.onNext(response);
		responseObserver.onCompleted();
	}

}
