package com.gcash.cart.service.impl;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriComponentsBuilder;

import com.gcash.cart.dto.Checkout;
import com.gcash.cart.dto.InventoryRequest;
import com.gcash.cart.dto.CartResponse;
import com.gcash.cart.dto.ResponseOrder;
import com.gcash.cart.enums.CartStatus;
import com.gcash.cart.exceptions.CartAlreadyPaidException;
import com.gcash.cart.exceptions.CartNotFoundException;
import com.gcash.cart.exceptions.InsufficientProductStockException;
import com.gcash.cart.grpc.GCartOrderRequest;
import com.gcash.cart.grpc.GCartOrderResponse;
import com.gcash.cart.grpc.GCheckoutCartRequest;
import com.gcash.cart.grpc.GDeleteFromCartRequest;
import com.gcash.cart.grpc.GOrderRequest;
import com.gcash.cart.grpc.GOrderResponse;
import com.gcash.cart.grpc.GUpdateCartRequest;
import com.gcash.cart.grpc.GUpdateOrderRequest;
import com.gcash.cart.repository.CartRepository;
import com.gcash.cart.repository.OrderRepository;
import com.gcash.cart.service.CartService;
import com.google.protobuf.ProtocolStringList;

import io.micrometer.common.util.StringUtils;

@Service("cartService")
public class CartImplService implements CartService{
	
	@Autowired
	private CartRepository cartRepository;
	
	@Autowired
	private OrderRepository orderRepository;
	
	@Autowired
	private ConversionService conversionService;
	
	@Autowired
	private WebClient webClient;
	
	@Value("${product.checkout.endpoint}")
	private String productCheckoutEndpoint;
	
	@Value("${product.validate.inventory.endpoint}")
	private String inventoryEndpoint;
	
	private static final String CART_NOT_FOUND_ERR_MESSAGE = "Cart not found: %s";

	@Override
	public GCartOrderResponse removeFromCart(GDeleteFromCartRequest request) {
		String cartId = request.getId();
		validateCartStatus(cartId);
		ProtocolStringList orderIds = request.getOrderIdsList();
		
		orderIds.stream().forEach(orderId -> orderRepository.deleteOrder(cartId, orderId));
		
		return viewCart(request.getId());
	}

	@Override
	public GCartOrderResponse checkoutCart(GCheckoutCartRequest request) {
		String cartId = request.getId();
		validateCartStatus(cartId);
		List<ResponseOrder> orders = orderRepository.getOrderByCartId(cartId);
		
		Checkout checkout = webClient.post()
				.uri(productCheckoutEndpoint)
				.contentType(MediaType.APPLICATION_JSON)
				.bodyValue(orders)
				.retrieve()
				.bodyToMono(Checkout.class)
				.block();
		
		if (!ObjectUtils.isEmpty(checkout)) {
			cartRepository.updateCart(cartId, CartStatus.PAID.toString());
		}
		
		return viewCart(cartId);
	}

	@Override
	public GCartOrderResponse viewCart(String cartId) {
		CartResponse cart = cartRepository.getCart(cartId);
		if (cart == null) {
			throw new CartNotFoundException(String.format(CART_NOT_FOUND_ERR_MESSAGE, cartId));
		}
		List<ResponseOrder> responseOrders = orderRepository.getOrderByCartId(cartId);
		List<GOrderResponse> gResponseOrders = responseOrders.stream()
				.map(responseOrder -> conversionService.convert(responseOrder, GOrderResponse.class))
				.toList();
		return GCartOrderResponse.newBuilder()
				.setId(cartId)
				.addAllOrders(gResponseOrders)
				.setStatus(cart.getStatus())
				.build();
	}

	@Transactional
	@Override
	public GCartOrderResponse addToCart(GCartOrderRequest request) {
		String cartId = request.getId();
		
		cartId = createCartIfNotPresent(cartId);
		validateCartStatus(cartId);
		
		List<GOrderRequest> productOrderRequest = request.getOrdersList();
		for (GOrderRequest order:  productOrderRequest) {
			if (isAvailable(order.getProductId(), order.getQuantity())) {
				orderRepository.createOrder(cartId, order);
			} else {
				throw new InsufficientProductStockException(String.format("Insufficient product stock: %s", order.getProductId()));
			}
		}
		
		return viewCart(cartId);
	}
	
	@Override
	public GCartOrderResponse updateCart(GUpdateCartRequest request) {
		String cartId = request.getId();
		validateCartStatus(cartId);
		List<GUpdateOrderRequest> orderList = request.getOrdersList();
		orderList.stream().forEach(order -> orderRepository.updateOrder(cartId, order.getId(), order.getQuantity()));
		
		return viewCart(request.getId());
	}
	
	/**
	 * Create a Cart if it does not exist.
	 * ID is auto-generated.
	 * 
	 * @return cartId
	 * */
	private String createCartIfNotPresent(String cartId) {
		// Generate new Cart
		if (StringUtils.isBlank(cartId)) {
			cartId = UUID.randomUUID().toString();
			cartRepository.createCart(cartId);
			return cartId;
		} else {
			CartResponse cart = cartRepository.getCart(cartId);
			if (ObjectUtils.isEmpty(cart)) {
				throw new CartNotFoundException(String.format(CART_NOT_FOUND_ERR_MESSAGE, cartId));
			} else {
				return cartId;
			}
		}
	}
	
	private boolean isAvailable(String productId, int quantity) {
		String endpoint = UriComponentsBuilder
				.fromHttpUrl(inventoryEndpoint)
				.path("/")
				.path(productId)
				.path("/inventories")
				.path("/verification")
				.toUriString();
		
		InventoryRequest inventoryRequest = new InventoryRequest();
		inventoryRequest.setQuantity(quantity);
		
		return webClient.post()
				.uri(endpoint)
				.contentType(MediaType.APPLICATION_JSON)
				.bodyValue(inventoryRequest)
				.retrieve()
				.bodyToMono(Boolean.class)
				.block();
	}
	
	private void validateCartStatus(String cartId) {
		CartResponse cart = cartRepository.getCart(cartId);
		if (cart == null) {
			throw new CartNotFoundException(String.format(CART_NOT_FOUND_ERR_MESSAGE, cartId));
		} else if (cart.getStatus().equalsIgnoreCase(CartStatus.PAID.toString())) {
			throw new CartAlreadyPaidException(String.format("Cart is already paid: %s", cartId));
		}
	}
}
