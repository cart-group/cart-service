package com.gcash.cart.repository;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.gcash.cart.dto.Order;
import com.gcash.cart.dto.CartResponse;

@Mapper
public interface CartRepository {
	
	@Insert("insert into cart(cart_no,product_id,quantity,status) values(#{cartId},#{order.productId},#{order.quantity},#{order.status})")
	void insertCart(String cartId, Order order);
	
	@Insert("insert into cart(id,status) values(#{cartId},'UNPAID')")
	void createCart(@Param("cartId") String cartId);
	
	@Update("UPDATE cart SET status = #{status} WHERE id = #{cartId};")
	void updateCart(@Param("cartId") String cartId, @Param("status") String status);
	
	@Select("SELECT id, status FROM cart where id = #{cartId}")
	CartResponse getCart(@Param("cartId") String cartId);
}
