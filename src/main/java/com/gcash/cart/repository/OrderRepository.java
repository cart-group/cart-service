package com.gcash.cart.repository;

import java.util.List;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.UpdateProvider;

import com.gcash.cart.dto.ResponseOrder;
import com.gcash.cart.grpc.GOrderRequest;
import com.gcash.cart.util.OrderUtil;

@Mapper
public interface OrderRepository {
	@InsertProvider(type = OrderUtil.class, method = "buildInsertOrderQuery")
	void createOrder(String cartId, GOrderRequest order);
	
	@Select("SELECT id,product_id AS productId,quantity FROM `order` where cart_id = #{cartId}")
	List<ResponseOrder> getOrderByCartId(@Param("cartId") String cartId);
	
	@InsertProvider(type = OrderUtil.class, method = "buildDeleteOrderQuery")
	void deleteOrder(String cartId, String orderId);
	
	@UpdateProvider(type = OrderUtil.class, method = "buildUpdateOrderQuery")
	void updateOrder(String cartId, String orderId, int quantity);
	
	
}
