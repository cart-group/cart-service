package com.gcash.cart.util;

import org.apache.ibatis.jdbc.SQL;

import com.gcash.cart.grpc.GOrderRequest;

public class OrderUtil {
	private static final String ORDER_TABLE = "`order`";
	private static final String CART_ID_COLUMN = "cart_id";
	private static final String ID_COLUMN_ASSIGNMENT = "id =";
	private static final String CART_ID_COLUMN_ASSIGNMENT = "cart_id =";
	private static final String PRODUCT_ID_COLUMN = "product_id";
	private static final String QUANTITY_COLUMN = "quantity";
	
	public String buildInsertOrderQuery(String cartId, GOrderRequest order) {
		return new SQL().
			INSERT_INTO(ORDER_TABLE)
			.VALUES("id", "UUID()")
			.VALUES(CART_ID_COLUMN, formatStringValue(cartId))
			.VALUES(PRODUCT_ID_COLUMN, formatStringValue(order.getProductId()))
			.VALUES(QUANTITY_COLUMN, order.getQuantity() + "").toString();
	}
	
	public String buildDeleteOrderQuery(String cartId, String orderId) {
		return new SQL()
				.DELETE_FROM(ORDER_TABLE)
				.WHERE(CART_ID_COLUMN_ASSIGNMENT + formatStringValue(cartId), ID_COLUMN_ASSIGNMENT + formatStringValue(orderId))
				.toString();
	}
	
	public String buildUpdateOrderQuery(String cartId, String orderId, int quantity) {
		return new SQL()
				.UPDATE(ORDER_TABLE)
				.SET("quantity = " + quantity)
				.WHERE(CART_ID_COLUMN_ASSIGNMENT + formatStringValue(cartId), ID_COLUMN_ASSIGNMENT + formatStringValue(orderId))
				.toString();
	}
	
	private String formatStringValue(String value) {
		return "'" + value + "'";
	}
	
	
}
