package com.gcash.cart.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.gcash.cart.converter.GCartOrderRequestToCartOrderRequestConverter;
import com.gcash.cart.converter.ResponseOrderToGOrderResponseConverter;

@Configuration
public class WebConfig implements WebMvcConfigurer {

	@Override
	public void addFormatters(FormatterRegistry registry) {
		registry.addConverter(new GCartOrderRequestToCartOrderRequestConverter());
		registry.addConverter(new ResponseOrderToGOrderResponseConverter());
	}

}
