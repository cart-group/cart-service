package com.gcash.cart.exceptions;

public class CartAlreadyPaidException extends RuntimeException {
	private static final long serialVersionUID = -2652315541284877391L;

	public CartAlreadyPaidException(String message) {
		super(message);
	}

}
