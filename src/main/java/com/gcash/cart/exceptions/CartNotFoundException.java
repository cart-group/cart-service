package com.gcash.cart.exceptions;

public class CartNotFoundException extends RuntimeException {
	private static final long serialVersionUID = 7079701005644348294L;
	
	public CartNotFoundException(String message) {
		super(message);
	}

}
