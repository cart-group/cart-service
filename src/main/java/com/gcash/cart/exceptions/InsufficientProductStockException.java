package com.gcash.cart.exceptions;

public class InsufficientProductStockException extends RuntimeException {
	private static final long serialVersionUID = 733467261962467227L;

	public InsufficientProductStockException(String message) {
		super(message);
	}

}
