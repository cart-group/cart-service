package com.gcash.cart.exceptions;

import java.util.Optional;

import org.springframework.web.reactive.function.client.WebClientResponseException;

import io.grpc.Status;
import net.devh.boot.grpc.server.advice.GrpcAdvice;
import net.devh.boot.grpc.server.advice.GrpcExceptionHandler;

@GrpcAdvice
public class GrpcExceptionAdvice {

	@GrpcExceptionHandler(CartNotFoundException.class)
	public Status handleCartNotFoundException(CartNotFoundException e) {
		return Status.NOT_FOUND.withDescription(e.getMessage()).withCause(e);
	}
	
	@GrpcExceptionHandler(InsufficientProductStockException.class)
	public Status handleInsufficientProductStockException(InsufficientProductStockException e) {
		return Status.ABORTED.withDescription(e.getMessage()).withCause(e);
	}
	
	@GrpcExceptionHandler(CartAlreadyPaidException.class)
	public Status handleCartAlreadyPaidException(CartAlreadyPaidException e) {
		return Status.INTERNAL.withDescription(e.getMessage()).withCause(e);
	}
	
	@GrpcExceptionHandler(WebClientResponseException.class)
	public Status handleWebClientResponseException(WebClientResponseException e) {
		String error = "";
		ErrorResponse errorResponse = Optional.ofNullable(e.getResponseBodyAs(ErrorResponse.class)).orElse(null);
		if (errorResponse == null) {
			error = "";
		} else {
			error = errorResponse.getError();
		}
		
		return Status.INTERNAL.withDescription(error).withCause(e);
	}
}
