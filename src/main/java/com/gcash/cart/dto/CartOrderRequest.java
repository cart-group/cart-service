package com.gcash.cart.dto;

import java.util.List;

import lombok.Data;

@Data
public class CartOrderRequest {
	private String cartId;
	
	private List<OrderRequest> orders;
}
