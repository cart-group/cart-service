package com.gcash.cart.dto;

import java.util.UUID;

import lombok.Data;

@Data
public class CheckoutRequest {
	private UUID productId;
	
	private int quantity;
}
