package com.gcash.cart.dto;

import lombok.Data;

@Data
public class InventoryRequest {
	private int quantity;
}
