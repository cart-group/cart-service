package com.gcash.cart.dto;

import lombok.Data;

@Data
public class ResponseOrder {
	private String id;
	
	private String productId;
	
	private int quantity;
}
