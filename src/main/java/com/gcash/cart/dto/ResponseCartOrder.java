package com.gcash.cart.dto;

import java.util.List;

import lombok.Data;

@Data
public class ResponseCartOrder {
	private String cartId;
	
	private String status;
	
	List<ResponseOrder> orders;
}
