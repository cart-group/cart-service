package com.gcash.cart.dto;

import java.util.UUID;

import lombok.Data;

@Data
public class Order {
	private UUID productId;
	
	private UUID cartId;
	
	private int quantity;
}
