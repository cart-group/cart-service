package com.gcash.cart.dto;

import java.util.List;
import java.util.UUID;

import com.gcash.cart.enums.CartStatus;

import lombok.Data;

@Data
public class Cart {
	private UUID id;
	
	private CartStatus status;
	
	List<Order> orders;
}
