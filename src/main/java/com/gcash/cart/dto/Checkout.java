package com.gcash.cart.dto;

import java.util.List;
import java.util.UUID;
import lombok.Data;

@Data
public class Checkout {
	private UUID id;
	
	private List<CheckoutRequest> checkoutOrders;
}
