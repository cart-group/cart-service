FROM eclipse-temurin:17-jre-alpine
VOLUME /tmp
RUN mkdir /opt/app
COPY target/*.jar /opt/app/cart.jar
EXPOSE 9994
CMD [ "java", "-jar", "/opt/app/cart.jar"]