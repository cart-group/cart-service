# cart-service



## Docker Image
Docker Repository:

```
https://hub.docker.com/r/villetorio/cart-service
```

Docker pull

```
docker pull villetorio/cart-service
```
## Run via docker compose
```
version: '3.1'
services:
  cart:
    build: .
    ports:
      - "9994:9994"
    environment:
      grpc.server.port: "9090"
      spring.datasource.url: "jdbc:mysql://db:3306/carts?createDatabaseIfNotExists=true&useSSL=false&allowPublicKeyRetrieval=true&autoReconnect=true"
      spring.datasource.username: "product_user"
      spring.datasource.password: "pass"
      product.checkout.endpoint: "http://product-service:9993/checkout"
  
networks: 
  default: 
    external: 
      name: external-bridge  
```

## K8s Deployment/Service
```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: cart
spec:
  selector:
    matchLabels:
      app: cart
  template:
    metadata:
      labels:
        app: cart
    spec:
      containers:
      - name: cart
        image: villetorio/cart-service:1.0.0
        livenessProbe:
          httpGet:
            path: "/actuator/health/liveness"
            port: 9994
          initialDelaySeconds: 30
          periodSeconds: 10
        readinessProbe:
          httpGet:
            path: "/actuator/health/readiness"
            port: 9994
          initialDelaySeconds: 30
          periodSeconds: 10
        resources:
          limits:
            memory: "512Mi"
            cpu: "500m"
        ports:
        - containerPort: 9994
        env:
          - name: grpc.server.port
            value: "9090"
          - name: spring.datasource.url
            value: "jdbc:mysql://mysqldb:3306/carts?createDatabaseIfNotExists=true&useSSL=false&allowPublicKeyRetrieval=true&autoReconnect=true"
          - name: spring.datasource.username
            value: product_user
          - name: spring.datasource.password
            value: pass
          - name: product.checkout.endpoint
            value: "http://product-service:9993/checkout"
---
apiVersion: v1
kind: Service
metadata:
  name: cart-service
spec:
  type: LoadBalancer
  selector:
    app: cart
  ports:
  - port: 9994
    targetPort: 9994
```
